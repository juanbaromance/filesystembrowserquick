#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QStandardPaths>

#include <QQuickView>
#include <QQmlContext>
#include "cpp/sharedApp.hpp"

template<class T> T qmlAttach( QQmlContext *q ){ return T(q); }

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    app.setOrganizationName("Sedecal");
    app.setOrganizationDomain("Sedecal.com");
    app.setApplicationName("FileSystemBrowser");

    QQmlApplicationEngine engine;

    // The objects must be inyected into the framework before QML code be loaded,
    // sources expect to drain c++ data on the QML deployment phase
    auto tmp = qmlAttach<sharedApp>(engine.rootContext());

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.load(url);

#if defined(Q_OS_ANDROID)
    QObject::connect(&app, SIGNAL(applicationStateChanged(Qt::ApplicationState)), &tmp, SLOT(onApplicationStateChanged(Qt::ApplicationState)));
#endif

    return app.exec();
}






