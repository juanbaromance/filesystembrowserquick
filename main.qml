import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import Qt.labs.folderlistmodel 2.12
import Qt.labs.platform 1.0
import QtQuick.Controls.Material 2.12
import "common"

ApplicationWindow {
    id: appWindow

    width: 480
    height: 480
    visible: true
    title: qsTr("File System Browser")
    property string mtime;
    property color dropShadow: Material.dropShadowColor
    property color primaryColor: Material.primary
    readonly property int preViewPage : 1;
    readonly property int fileBrowserPage : 0;

    function timeChanged() {
        var date = new Date;
        function zeroed(num){ return String(num).padStart(2, '0'); }
        mtime = zeroed(date.getHours()) + ":" + zeroed(date.getMinutes()) + ":" + zeroed(date.getUTCSeconds());
    }

    Timer {
        interval: 1000; running: true; repeat: true;
        onTriggered: timeChanged()
    }

    FloatingActionButton {
        id: fab
        property string imageName: "arrow_back.png"
        z: 1
        anchors.margins: 10
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        imageSource: "qrc:/Images/"+imageName
        opacity: 0.7
        // backgroundColor: primaryDarkColor
        onClicked:
            if( swipeView.currentIndex === fileBrowserPage )
                fileSystemBrowserId.folderSel += "/.."
            else
                swipeView.setCurrentIndex(fileBrowserPage)
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        anchors.margins: 2

        Page {
            id: fileSystemPage
            property string textContent: ""
            property string imageContent: ""
            property var fileSelection: ""
            visible: swipeView.currentIndex === fileBrowserPage ? true : false

            FileSystemBrowser {
                id: fileSystemBrowserId
                property real androidPreview: 1.0
                displayMarginBeginning: -130
                onAcceptedSelection:
                {
                    fileSystemPage.fileSelection=fileName
                    fileSystemPage.textContent=fileName
                    //var ret_val = Qt.openUrlExternally(fileName)
                    //if( ret_val === false )
                    if (  modePreview === "External" )
                        shareUtils.viewFile( myApp.syncSharedFolder(fileName), "Preview " + fileName );
                    else
                        swipeView.setCurrentIndex(preViewPage);
                }
            }
        }

        Page {
            PreView {
                textContent: fileSystemPage.textContent
                imageContent: fileSystemPage.imageContent
                fileName: fileSystemPage.fileSelection
                onAcceptedSelection: swipeView.setCurrentIndex(fileBrowserPage)
            }
        }
    }

    SharedAppProcessor { }
}
