#ifndef CROMREFLECTOR_H
#define CROMREFLECTOR_H

#include <QObject>
#include <memory>

class QQmlContext;
class cROMreflectorImpl;
class cROMreflector : public QObject
{
    Q_OBJECT
public:
    explicit cROMreflector(QQmlContext *context, QObject *parent = nullptr);
    Q_PROPERTY(QString external_storage READ external_storage )
    Q_INVOKABLE bool rebuild();
    ~cROMreflector();

signals:

public slots:
    QString external_storage() const;

private:
    QString m_external_storage;   
    std::unique_ptr<cROMreflectorImpl> impl;
};

#endif // CROMREFLECTOR_H
