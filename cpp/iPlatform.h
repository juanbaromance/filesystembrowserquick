#pragma once
#include <QObject>
#include <QDebug>
#include <QtQml>

class SharedAppEnums {
    Q_GADGET
public:
    explicit SharedAppEnums(){}
    enum SharedTasksId {
        UnknownId = -1,
        SendId = 1001,
        ViewId = 1002,
        EditId = 1003
    };
    Q_ENUM(SharedTasksId)
};

// TODO: JB#07122020 Pure virtualization to get an interface
// Null Pattern to solve a dummy platform
class PlatformShareUtils : public QObject
{

    Q_OBJECT
signals:
    void shareEditDone(int requestCode);
    void shareFinished(int requestCode);
    void shareNoAppAvailable(int requestCode);
    void shareError(QString message, int requestCode = 0 );
    void fileUrlReceived(QString url);
    void fileReceivedAndSaved(QString url);

public:

    PlatformShareUtils( QObject *parent = nullptr ) : QObject(parent){
        qmlRegisterUncreatableType<SharedAppEnums>("FileSystemBrowser",1,0,"SharedAppEnums", "Can not create SharedAppEnums type in QML. Not allowed.");
    }
    virtual ~PlatformShareUtils() {}
    virtual bool checkMimeTypeView(const QString &mimeType){ qDebug() << "check view for " << mimeType; return true;}
    virtual bool checkMimeTypeEdit(const QString &mimeType){ qDebug() << "check edit for " << mimeType; return true; }
    virtual void share(const QString &text, const QUrl &url){ qDebug() << text << url; }
    virtual void sendFile(const QString &filePath, const QString &title= "Send", const QString &mimeType = ""){  qDebug() << filePath << " - " << title << "mt(" << mimeType; }
    virtual bool viewFile(const QString &filePath, const QString &title= "View", const QString &mimeType = ""){  return false; qDebug() << filePath << " - " << title << " " << mimeType; }
    virtual void editFile(const QString &filePath, const QString &title= "Edit", const QString &mimeType = ""){ qDebug() << filePath << " - " << title << " " << mimeType ; }
    virtual void checkPendingIntents(const QString workingDirPath){ qDebug() << "checkPendingIntents " << workingDirPath; }
    virtual QString auditory(void) { return "nothing to audit"; }
};
