#pragma once
// (c) 2017 Ekkehard Gentz (ekke)
// this project is based on ideas from
// http://blog.lasconic.com/share-on-ios-and-android-using-qml/
// see github project https://github.com/lasconic/ShareUtils-QML
// also inspired by:
// https://www.androidcode.ninja/android-share-intent-example/
// https://www.calligra.org/blogs/sharing-with-qt-on-android/
// https://stackoverflow.com/questions/7156932/open-file-in-another-app
// http://www.qtcentre.org/threads/58668-How-to-use-QAndroidJniObject-for-intent-setData
// see also /COPYRIGHT and /LICENSE

// (c) 2017 Ekkehard Gentz (ekke) @ekkescorner
// my blog about Qt for mobile: http://j.mp/qt-x
// see also /COPYRIGHT and /LICENSE

// JB#07122020 Large refactor using c++ modernes/ec++/ and SOLID technics

#include <QObject>
#include <QDebug>

class PlatformShareUtils;

// QML main interface ala proxy on the forward and backward channels
class ShareUtils : public QObject
{
    Q_OBJECT

public:
    explicit ShareUtils(QObject *parent = 0);
    virtual ~ShareUtils();
    Q_INVOKABLE bool checkMimeTypeView(const QString &mimeType);
    Q_INVOKABLE bool checkMimeTypeEdit(const QString &mimeType);
    Q_INVOKABLE void share(const QString &text, const QUrl &url);
    Q_INVOKABLE void sendFile(const QString &filePath, const QString &title = "Send", const QString &mimeType = "" );
    Q_INVOKABLE bool viewFile(const QString &filePath, const QString &title = "View", const QString &mimeType = "" );
    Q_INVOKABLE void editFile(const QString &filePath, const QString &title = "Edit", const QString &mimeType = "" );
    Q_INVOKABLE void checkPendingIntents(const QString workingDirPath);
    Q_INVOKABLE QString auditory();

private:
    PlatformShareUtils* platftorm;

    // platform reflectors for the QML side in charge to monitor/process the OS reactions
signals:
    void shareEditDone(int requestCode);
    void shareFinished(int requestCode);
    void shareNoAppAvailable(int requestCode);
    void shareError(QString message, int requestCode = 0 );
    void fileUrlReceived(QString url);
    void fileReceivedAndSaved(QString url);

};
