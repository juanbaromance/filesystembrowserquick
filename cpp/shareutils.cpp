// (c) 2017 Ekkehard Gentz (ekke) @ekkescorner
// my blog about Qt for mobile: http://j.mp/qt-x
// see also /COPYRIGHT and /LICENSE

#include "shareutils.hpp"

#ifdef Q_OS_IOS
#include "cpp/ios/iosshareutils.hpp"
#else
#ifdef Q_OS_ANDROID
#include "cpp/android/androidshareutils.hpp"
#else
#endif
#endif

#include "cpp/iPlatform.h"

ShareUtils::ShareUtils(QObject *parent) : QObject(parent)
{

#if defined(Q_OS_IOS)
    mPlatformShareUtils = new IosShareUtils(this);
#elif defined(Q_OS_ANDROID)
    platftorm = new AndroidShareUtils(this);
#else
    platftorm = new PlatformShareUtils(this);
#endif

    connect( platftorm, &PlatformShareUtils::shareEditDone, [&]( int requestCode ){ emit shareEditDone(requestCode); } );
    connect( platftorm, &PlatformShareUtils::shareError   , [&]( QString message, int requestCode ){ emit shareError(message, requestCode ); });
    connect( platftorm, &PlatformShareUtils::shareNoAppAvailable, [&](int requestCode){ emit shareNoAppAvailable(requestCode); });
    connect( platftorm, &PlatformShareUtils::shareFinished, [&](int requestCode){ emit shareFinished(requestCode);} );

}
void ShareUtils::checkPendingIntents(const QString workingDirPath){ platftorm->checkPendingIntents(workingDirPath); }
QString ShareUtils::auditory(){ return platftorm->auditory(); }

/* This destructor allows future refactor on unique_ptr to ride off the delete stuff */
ShareUtils::~ShareUtils(){ delete platftorm; }

bool ShareUtils::checkMimeTypeView(const QString &mimeType){ return platftorm->checkMimeTypeView(mimeType);}
bool ShareUtils::checkMimeTypeEdit(const QString &mimeType){ return platftorm->checkMimeTypeEdit(mimeType);}
void ShareUtils::share    ( const QString &text, const QUrl &url){ platftorm->share(text, url); }
void ShareUtils::sendFile ( const QString &filePath, const QString &title, const QString &mimeType){ platftorm->sendFile(filePath, title, mimeType ); }
bool ShareUtils::viewFile ( const QString &filePath, const QString &title, const QString &mimeType){ return platftorm->viewFile(filePath, title, mimeType ); }
void ShareUtils::editFile ( const QString &filePath, const QString &title, const QString &mimeType){ platftorm->editFile(filePath, title, mimeType ); }



