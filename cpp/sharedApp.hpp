// (c) 2017 Ekkehard Gentz (ekke) @ekkescorner
// my blog about Qt for mobile: http://j.mp/qt-x
// see also /COPYRIGHT and /LICENSE
#ifndef APPLICATIONUI_HPP
#define APPLICATIONUI_HPP

#include <QObject>
#include <QtQml>

class cROMreflector;
class ShareUtils;
class FileIO;

class sharedApp : public QObject
{
    Q_OBJECT
public:
     sharedApp( QQmlContext *context, QObject *parent = 0 );
     Q_INVOKABLE QString syncSharedFolder( const QString & view_file );
     Q_INVOKABLE bool    recycleSharedFolder(const QString & folder = "", const int requestCode = -1 );
     Q_INVOKABLE bool    updateFileFromDocumentsLocation(const int requestId);
     Q_INVOKABLE bool    checkPermission();
     virtual ~sharedApp();

signals:
     void noDocumentsWorkLocation();

public slots:
     void onApplicationStateChanged(Qt::ApplicationState applicationState);

private:
     ShareUtils* mShareUtils;
     std::unique_ptr<cROMreflector> rom_reflector;
     std::unique_ptr<FileIO> file_io;
     bool mPendingIntentsChecked; // TODO: stucked on true wtf!
     QString mAppDataFilesPath;
     QString mDocumentsWorkPath;
     void createSharedFolder();
};

#endif // APPLICATIONUI_HPP
