// (c) 2017 Ekkehard Gentz (ekke) @ekkescorner
// my blog about Qt for mobile: http://j.mp/qt-x
// see also /COPYRIGHT and /LICENSE

#include <QtQml>
#include <QGuiApplication>

#include <QFile>
#include <QDir>
#include <QDebug>

#if defined(Q_OS_ANDROID)
#include <QtAndroid>
#endif

#include "cpp/shareutils.hpp"
#include "sharedApp.hpp"
#include "iPlatform.h"
#include <cROMreflector.h>
#include <memory>
#include "fileIO.h"

sharedApp::sharedApp(QQmlContext *context, QObject *parent) : QObject(parent), mShareUtils( new ShareUtils(this) )
{
    createSharedFolder();
    file_io       = std::make_unique<FileIO>();
    rom_reflector = std::make_unique<cROMreflector>(context);

    // c++ exposition to QML
    context->setContextProperty( "myApp", this );
    context->setContextProperty( "shareUtils", mShareUtils);
    context->setContextProperty( "fileIO", file_io.get());
    qmlRegisterType<FileIO, 1>("FileIO", 1, 0, "FileIO");

}

void sharedApp::createSharedFolder()
{
    QString appDataRoot = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0);
    qDebug() << "QStandardPaths::AppDataLocation: " << appDataRoot;

    // as next we create a /my_share_files subdirectory to store our example files from assets
    mAppDataFilesPath = appDataRoot.append("/my_share_files");
    if (!QDir(mAppDataFilesPath).exists()) {
        if (QDir("").mkpath(mAppDataFilesPath)) {
            qDebug() << "Created app data /files directory. " << mAppDataFilesPath;
        } else {
            qWarning() << "Failed to create app data /files directory. " << mAppDataFilesPath;
            return;
        }
    }

    QString docLocationRoot = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0);
    mDocumentsWorkPath = docLocationRoot.append("/share_example_x_files");
    if (!QDir(mDocumentsWorkPath).exists()) {
        if (QDir("").mkpath(mDocumentsWorkPath)) {
            qDebug() << "Created Documents Location work directory. " << mDocumentsWorkPath;
        } else {
            qWarning() << "Failed to create Documents Location work directory. " << mDocumentsWorkPath;
            return;
        }
    }

    for( auto f : {mAppDataFilesPath, mDocumentsWorkPath} )
        recycleSharedFolder(f);

}

QString sharedApp::syncSharedFolder( const QString & view_file ) {

    QString filename{QUrl(view_file).toLocalFile().toStdString().c_str()};
    QFile f( filename );
    if( f.exists() )
        if( f.open(QIODevice::ReadOnly) )
        {
            QString target = mDocumentsWorkPath + "/" + QFileInfo(view_file).fileName();
                if( QFile t(target); t.exists() )
                    t.remove();
            if( f.copy( target ) )
                return target;
            emit mShareUtils->shareError(f.errorString());
        }

    emit mShareUtils->shareError(f.errorString());
    return "";
}

bool sharedApp::recycleSharedFolder( const QString & folder, const int requestCode )
{
    QDir dir( folder.length() == 0 ? mDocumentsWorkPath : folder );
    qDebug() << "cleanup on " << QFileInfo(dir.dirName());
    foreach( QString f, dir.entryList())
        dir.remove(f);
    return true;
}

bool sharedApp::updateFileFromDocumentsLocation(const int requestCode )
{
    qDebug() << "File(" << requestCode <<") updated from Documents Location";
    return true;
}

sharedApp::~sharedApp(){}

void sharedApp::onApplicationStateChanged( Qt::ApplicationState applicationState )
{
    qDebug() << "S T A T E changed into: " << applicationState;
    if( applicationState == Qt::ApplicationState::ApplicationActive ) {
        if( mPendingIntentsChecked == false )
        {
            mPendingIntentsChecked = true;
            mShareUtils->checkPendingIntents(mAppDataFilesPath);
        }
    }
}


// we don't need permissions if we only share files to other apps using FileProvider
// but we need permissions if other apps share their files with out app and we must access those files
bool sharedApp::checkPermission() {

#if defined(Q_OS_ANDROID)
    QtAndroid::PermissionResult r = QtAndroid::checkPermission("android.permission.WRITE_EXTERNAL_STORAGE");
    if( r == QtAndroid::PermissionResult::Denied ) {
        QtAndroid::requestPermissionsSync( QStringList() << "android.permission.WRITE_EXTERNAL_STORAGE" );
        r = QtAndroid::checkPermission("android.permission.WRITE_EXTERNAL_STORAGE");
        if(r == QtAndroid::PermissionResult::Denied) {
            qDebug() << "Permission denied";
            emit noDocumentsWorkLocation();
            return false;
        }
   }
    qDebug() << "YEP: Permission OK";
#endif
    return true;

}
