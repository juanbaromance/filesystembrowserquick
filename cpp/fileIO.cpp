#include "fileIO.h"
#include <QFile>
#include <QTextStream>

FileIO::FileIO(QObject *parent) : QObject(parent){}

QString FileIO::read()
{
    if ( mSource.isEmpty()){
        QString err("source is empty");
        emit error(err);
        return err;
    }

    QFile file(mSource);
    QString fileContent;
    if ( file.open( QIODevice::ReadOnly ) ) {
        QString line;
        QTextStream t( &file );
        do {
            line = t.readLine();
            fileContent += line + ( line.isNull() ? "" : "\n" );
         } while (!line.isNull());

        file.close();
    } else {
        emit error("Unable to open the file " + mSource );
        return file.errorString();
    }
    return fileContent;
}


bool FileIO::write(const QString& data)
{
    if (mSource.isEmpty())
        return false;

    QFile file(mSource);
    if (!file.open(QFile::WriteOnly | QFile::Truncate))
        return false;

    QTextStream out(&file);
    out << data;

    file.close();

    return true;
}

int FileIO::sizeOf() const
{
    return m_sizeOf;
}

#include <cmath>
#include <QFileInfo>
FileIO &FileIO::setURL( const QString &source )
{
    mSource = QUrl(source).toLocalFile();
    m_extension = QFileInfo(mSource).completeSuffix();
    m_sizeOf = static_cast<int>( round( QFileInfo(mSource).size()/1024. ) );
    return *this;
}
