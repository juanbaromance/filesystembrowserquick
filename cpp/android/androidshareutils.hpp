// (c) 2017 Ekkehard Gentz (ekke) @ekkescorner
// my blog about Qt for mobile: http://j.mp/qt-x
// see also /COPYRIGHT and /LICENSE

#ifndef ANDROIDSHAREUTILS_H
#define ANDROIDSHAREUTILS_H

#include <QtAndroid>
#include <QAndroidActivityResultReceiver>

#include "cpp/iPlatform.h"
#include "cpp/shareutils.hpp"

class AndroidShareUtils : public PlatformShareUtils, public QAndroidActivityResultReceiver
{
    /* The PlatformShareUtils contract: aka ISP */
public:
    bool checkMimeTypeView(const QString &mimeType) override;
    bool checkMimeTypeEdit(const QString &mimeType) override;
    void share(const QString &text, const QUrl &url) override;
    void sendFile(const QString &filePath, const QString &title = "Send", const QString &mimeType = "" ) override;
    bool viewFile(const QString &filePath, const QString &title = "View", const QString &mimeType = "" ) override;
    void editFile(const QString &filePath, const QString &title = "Edit", const QString &mimeType = "" ) override;
    void handleActivityResult(int receiverRequestCode, int resultCode, const QAndroidJniObject &data) override;
    void checkPendingIntents(const QString workingDirPath) override;
    QString auditory(void) override { return m_auditory; };

public:
    AndroidShareUtils( QObject* parent = nullptr);
    void onActivityResult(int requestCode, int resultCode);

public slots:
    void setFileUrlReceived(const QString &url);
    void setFileReceivedAndSaved(const QString &url);
    bool checkFileExits(const QString &url);

    // JB#06122020 TODO :: justify private here : aka DIP
private:
    bool mIsEditMode;
    int track;
    qint64 mLastModified;
    QString mCurrentFilePath, m_auditory;
    void processActivityResult(int requestCode, int resultCode);
    void error( const QString & s ){ qWarning() << ( m_auditory = s ); emit shareNoAppAvailable(track); };
    static constexpr const char* java_clazz = "org/ekkescorner/utils/QShareUtils";

    // JB#06122020 TODO :: rationale to justify the infamous singleton below
public:
    static AndroidShareUtils* getInstance();
private:
    static AndroidShareUtils* mInstance;
};



#endif // ANDROIDSHAREUTILS_H
