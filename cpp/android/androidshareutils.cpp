﻿// (c) 2017 Ekkehard Gentz (ekke) @ekkescorner
// my blog about Qt for mobile: http://j.mp/qt-x
// see also /COPYRIGHT and /LICENSE

#include "androidshareutils.hpp"

#include <QUrl>
#include <QFileInfo>
#include <QDateTime>

#include <QtAndroidExtras/QAndroidJniObject>
#include <jni.h>

static constexpr int RESULT_OK = -1;
static constexpr int RESULT_CANCELED = 0;
template <typename T> constexpr auto enumCollapse(T val) { return static_cast<std::underlying_type_t<T>>(val); };

AndroidShareUtils* AndroidShareUtils::mInstance = nullptr;
AndroidShareUtils::AndroidShareUtils(QObject* parent) : PlatformShareUtils(parent)
{ mInstance = this; }


bool AndroidShareUtils::checkMimeTypeView(const QString &mimeType)
{
    QAndroidJniObject jsMime = QAndroidJniObject::fromString(mimeType);
    jboolean verified = QAndroidJniObject::callStaticMethod<jboolean>
            (java_clazz,"checkMimeTypeView","(Ljava/lang/String;)Z",
             jsMime.object<jstring>());
    qDebug() << "View VERIFIED: " << mimeType << " - " << verified;
    return verified;
}

bool AndroidShareUtils::checkMimeTypeEdit(const QString &mimeType)
{
    QAndroidJniObject jsMime = QAndroidJniObject::fromString(mimeType);
    jboolean verified = QAndroidJniObject::callStaticMethod<jboolean>
            (java_clazz,"checkMimeTypeEdit","(Ljava/lang/String;)Z",
             jsMime.object<jstring>());
    qDebug() << "Edit VERIFIED: " << mimeType << " - " << verified;
    return verified;
}

void AndroidShareUtils::share(const QString &text, const QUrl &url)
{
    QAndroidJniObject jsText = QAndroidJniObject::fromString(text);
    QAndroidJniObject jsUrl = QAndroidJniObject::fromString(url.toString());
    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>
            (java_clazz,"share","(Ljava/lang/String;Ljava/lang/String;)Z",
             jsText.object<jstring>(), jsUrl.object<jstring>());

    if(!ok) {
        qWarning() << "Unable to resolve activity from Java";
        emit shareNoAppAvailable(0);
    }
}

void AndroidShareUtils::sendFile(const QString &filePath, const QString &title, const QString &mimeType )
{
    mIsEditMode = false;
    QAndroidJniObject jsPath = QAndroidJniObject::fromString(filePath);
    QAndroidJniObject jsTitle = QAndroidJniObject::fromString(title);
    QAndroidJniObject jsMimeType = QAndroidJniObject::fromString(mimeType);
    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>
            (java_clazz, "sendFile", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z",
             jsPath.object<jstring>(), jsTitle.object<jstring>(), jsMimeType.object<jstring>(),
             track = enumCollapse(SharedAppEnums::SendId));
    if(!ok) {
        qWarning() << "Unable to resolve activity from Java";
        emit shareNoAppAvailable( track );
    }
    return;

}

/* TODO:: JB@@07122020 refactor bug prony callStaticMethod with some metaprogramming technic */
#include <QMimeDatabase>
QString guessMimeType( const QString &filePath ){ return QMimeDatabase().mimeTypeForFile( filePath ).name(); }

constexpr const char *JavaString ="Ljava/lang/String;";
bool AndroidShareUtils::viewFile(const QString &filePath, const QString &title, const QString &mimeType )
{
    m_auditory.clear();
    if( filePath.length() == 0 )
        return false;

    static constexpr const char* view_method{"viewFile"};
    mIsEditMode = false;

    QAndroidJniObject jsPath = QAndroidJniObject::fromString(filePath);
    QAndroidJniObject jsTitle = QAndroidJniObject::fromString(title);
    QAndroidJniObject jsMimeType = QAndroidJniObject::fromString(mimeType.length() == 0 ? guessMimeType(filePath) : mimeType);

    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>
            (java_clazz, view_method, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z",
             jsPath.object<jstring>(), jsTitle.object<jstring>(), jsMimeType.object<jstring>(),
             track = enumCollapse(SharedAppEnums::ViewId));
    if(!ok)
        error( QString("<b>FAILURED</b> on Java s-method %1::%2 <br> (%3)")
               .arg(java_clazz).arg(view_method).arg(QFileInfo(filePath).fileName()));
    return ok;
}

void AndroidShareUtils::editFile(const QString &filePath, const QString &title, const QString &mimeType )
{

    mIsEditMode = true;
    mCurrentFilePath = filePath;
    QFileInfo fileInfo = QFileInfo(mCurrentFilePath);
    mLastModified = fileInfo.lastModified().toSecsSinceEpoch();
    qDebug() << "LAST MODIFIED: " << mLastModified;

    QAndroidJniObject jsPath = QAndroidJniObject::fromString(filePath);
    QAndroidJniObject jsTitle = QAndroidJniObject::fromString(title);
    QAndroidJniObject jsMimeType = QAndroidJniObject::fromString(mimeType);

    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>
            (java_clazz,"editFile","(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z",
             jsPath.object<jstring>(),
             jsTitle.object<jstring>(),
             jsMimeType.object<jstring>(),
             track = enumCollapse(SharedAppEnums::EditId));

    if(!ok) {
        qWarning() << "Unable to resolve activity from Java";
        emit shareNoAppAvailable(track);
    }
    return;

}

// used from QAndroidActivityResultReceiver
void AndroidShareUtils::handleActivityResult(int receiverRequestCode, int resultCode, const QAndroidJniObject &data)
{
    Q_UNUSED(data);
    qDebug() << "From JNI QAndroidActivityResultReceiver: " << receiverRequestCode << "ResultCode:" << resultCode;
    processActivityResult(receiverRequestCode, resultCode);
}


// Attention: not all Apps will give you the correct ResultCode:
// Google Fotos will send OK if saved and CANCELED if canceled
// Some Apps always sends CANCELED even if you modified and Saved the File
// so you should check the modified Timestamp of the File to know if
// you should emit shareEditDone() or shareFinished() !!!
void AndroidShareUtils::processActivityResult(int requestCode, int resultCode)
{
    if(resultCode == RESULT_OK) {
        emit shareEditDone(requestCode);
    } else if(resultCode == RESULT_CANCELED) {
        if(mIsEditMode) {
            QFileInfo fileInfo = QFileInfo(mCurrentFilePath);
            qint64 currentModified = fileInfo.lastModified().toSecsSinceEpoch();
            qDebug() << "CURRENT MODIFIED: " << currentModified;
            if(currentModified > mLastModified) {
                emit shareEditDone(requestCode);
                return;
            }
        }
        emit shareFinished(requestCode);
    } else {
        qDebug() << "wrong result code: " << resultCode << " from request: " << requestCode;
        emit shareError( tr("Share: an Error occured"), requestCode );
    }
}

void AndroidShareUtils::checkPendingIntents( const QString shared_path )
{
    QAndroidJniObject activity = QtAndroid::androidActivity();
    if(activity.isValid()) {
        QAndroidJniObject jparam = QAndroidJniObject::fromString(shared_path);
        if(!jparam.isValid()) {
            qWarning() << "QAndroidJniObject jniWorkingDir not valid.";
            emit shareError( tr("Share: an Error occured\nWorkingDir not valid"));
            return;
        }
        activity.callMethod<void>("checkPendingIntents","(Ljava/lang/String;)V",
                                  jparam.object<jstring>());
        qDebug() << "checkPendingIntents: " << shared_path;
        return;
    }
    qDebug() << "checkPendingIntents: Activity not valid";
}

// JB#06122020 Here is the singleton justification

// instead of defining all JNICALL as demonstrated below
// there's another way, making it easier to manage all the methods
// see https://www.kdab.com/qt-android-episode-5/

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL
  Java_org_ekkescorner_examples_sharex_QShareActivity_setFileUrlReceived(JNIEnv *env,
                                        jobject obj,
                                        jstring url)
{
    const char *urlStr = env->GetStringUTFChars(url, NULL);
    Q_UNUSED (obj)
    AndroidShareUtils::getInstance()->setFileUrlReceived(urlStr);
    env->ReleaseStringUTFChars(url, urlStr);
    return;
}

JNIEXPORT void JNICALL
  Java_org_ekkescorner_examples_sharex_QShareActivity_setFileReceivedAndSaved(JNIEnv *env,
                                        jobject obj,
                                        jstring url)
{
    const char *urlStr = env->GetStringUTFChars(url, NULL);
    Q_UNUSED (obj)
    AndroidShareUtils::getInstance()->setFileReceivedAndSaved(urlStr);
    env->ReleaseStringUTFChars(url, urlStr);
    return;
}

JNIEXPORT bool JNICALL
  Java_org_ekkescorner_examples_sharex_QShareActivity_checkFileExits(JNIEnv *env,
                                        jobject obj,
                                        jstring url)
{
    const char *urlStr = env->GetStringUTFChars(url, NULL);
    Q_UNUSED (obj)
    bool exists = AndroidShareUtils::getInstance()->checkFileExits(urlStr);
    env->ReleaseStringUTFChars(url, urlStr);
    return exists;
}

JNIEXPORT void JNICALL
  Java_org_ekkescorner_examples_sharex_QShareActivity_fireActivityResult(JNIEnv *env,
                                        jobject obj,
                                        jint requestCode,
                                        jint resultCode)
{
    Q_UNUSED (obj)
    Q_UNUSED (env)
    AndroidShareUtils::getInstance()->onActivityResult(requestCode, resultCode);
    return;
}

#ifdef __cplusplus
}
#endif

AndroidShareUtils* AndroidShareUtils::getInstance()
{ return ( mInstance == nullptr )  ? mInstance = new AndroidShareUtils : mInstance; }


void AndroidShareUtils::setFileUrlReceived(const QString &url)
{
    if(url.isEmpty()) {
        qWarning() << "setFileUrlReceived: we got an empty URL";
        emit shareError(tr("Empty URL received"));
        return;
    }
    qDebug() << "AndroidShareUtils setFileUrlReceived: we got the File URL from JAVA: " << url;
    QString myUrl;
    if(url.startsWith("file://")) {
        myUrl= url.right(url.length()-7);
        qDebug() << "QFile needs this URL: " << myUrl;
    } else {
        myUrl= url;
    }

    // check if File exists
    QFileInfo fileInfo = QFileInfo(myUrl);
    if(fileInfo.exists()) {
        emit fileUrlReceived(myUrl);
    } else {
        qDebug() << "setFileUrlReceived: FILE does NOT exist ";
        emit shareError(tr("File does not exist: %1").arg(myUrl));
    }
}

void AndroidShareUtils::setFileReceivedAndSaved(const QString &url)
{
    if(url.isEmpty()) {
        qWarning() << "setFileReceivedAndSaved: we got an empty URL";
        emit shareError(tr("Empty URL received"));
        return;
    }
    qDebug() << "AndroidShareUtils setFileReceivedAndSaved: we got the File URL from JAVA: " << url;
    QString myUrl;
    if(url.startsWith("file://")) {
        myUrl= url.right(url.length()-7);
        qDebug() << "QFile needs this URL: " << myUrl;
    } else {
        myUrl= url;
    }

    // check if File exists
    QFileInfo fileInfo = QFileInfo(myUrl);
    if(fileInfo.exists()) {
        emit fileReceivedAndSaved(myUrl);
    } else {
        qDebug() << "setFileReceivedAndSaved: FILE does NOT exist ";
        emit shareError( tr("File does not exist: %1").arg(myUrl));
    }
}

// to be safe we check if a File Url from java really exists for Qt
// if not on the Java side we'll try to read the content as Stream
bool AndroidShareUtils::checkFileExits( const QString &url )
{
    if(url.isEmpty()) {
        qWarning() << "checkFileExits: we got an empty URL";
        emit shareError( tr("Empty URL received"));
        return false;
    }
    qDebug() << "AndroidShareUtils checkFileExits: we got the File URL from JAVA: " << url;
    QString myUrl;
    if(url.startsWith("file://")) {
        myUrl= url.right(url.length()-7);
        qDebug() << "QFile needs this URL: " << myUrl;
    } else {
        myUrl= url;
    }

    // check if File exists
    QFileInfo fileInfo = QFileInfo(myUrl);
    if(fileInfo.exists()) {
        qDebug() << "Yep: the File exists for Qt";
        return true;
    } else {
        qDebug() << "Uuups: FILE does NOT exist ";
        return false;
    }
}

// used from Activity.java onActivityResult()
void AndroidShareUtils::onActivityResult(int requestCode, int resultCode)
{
    qDebug() << "From Java Activity onActivityResult: " << requestCode << "ResultCode:" << resultCode;
    processActivityResult(requestCode, resultCode);
}
