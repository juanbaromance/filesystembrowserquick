#ifndef FILEIO_H
#define FILEIO_H

#include <QObject>
#include <QUrl>
class FileIO : public QObject
{
    Q_OBJECT

public:
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged )
    Q_PROPERTY(QString extension READ extension )
    Q_PROPERTY(int     sizeOf READ sizeOf )
    Q_PROPERTY(QString url WRITE setURL )
    explicit FileIO(QObject *parent = 0);

    Q_INVOKABLE QString read();
    Q_INVOKABLE bool write(const QString& data);
    QString source() { return mSource; };
    QString extension() const{ return m_extension; }

    int sizeOf() const;

public slots:
    FileIO& setSource(const QString& source) { mSource = source; return *this;};
    FileIO& setURL(const QString& source);

signals:
    void sourceChanged(const QString& source);
    void error( const QString& msg );

private:
    QString mSource;
    QString m_extension;
    int m_sizeOf;
};



#endif // FILEIO_H
