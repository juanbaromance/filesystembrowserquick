import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12
import QtQuick 2.0
import QtQuick 2.6

ColumnLayout {

    id: rootPreviewId
    property var imageContent : ""
    property var textContent : ""
    property var fileName
    property var lastFileName: ""
    property var train_x : 0
    property var train_y : 0
    property int fontSizeInfo: 12
    property real opacityHeadline:0.7
    property int  fileSize: 0
    signal acceptedSelection( var fileName )

    visible: false
    width:parent.width;
    height:parent.height

    function init()
    {
        visible = false;
        imageContent = textContent = ""
    }

    Connections
    {
        target: swipeView
        onCurrentIndexChanged:
            if( swipeView.currentIndex === preViewPage  )
            {
                init();
                fileIO.url = fileName
                fileSize = fileIO.sizeOf
                console.log(mtime + ": Preview Scheduled : " + fileName )
                if( ["png","jpg","gif"].find( ext => ext === String(fileIO.extension).toLowerCase() ) ? true : false )
                {
                    imageContent = fileName;
                    if( lastFileName !== fileName )
                    {
                        lastFileName = fileName
                        pictureFrame.scale = 1;
                        pictureFrame.rotation = 0;
                        train_x = train_y = 0;
                        pictureFrame.x = pictureFrame.y = 0
                    }
                    // flick_view.contentX = train_x;
                    // flick_view.contentY = train_y;
                    imageView.visible = true;

                    // Dynamic binding
                    // zoomId.value = Qt.binding( function() { return pictureFrame.scale });
                }
                else
                {
                    imageView.visible = false;
                    textContent = fileIO.read();
                }
                visible = true;
            }
            else
            {
                train_x = flick_view.contentX;
                train_y = flick_view.contentY;
                visible = false;
            }
    }

    RowLayout {
        Layout.fillWidth: true;
        z :1

        Label {
            id:fileInfoId
            Layout.margins: 5
            opacity: opacityHeadline
            text: basename(String(fileName)) + "<br><small>" + fileSize + " Kbytes</small>"
            function basename(path) { return path.length > 0 ? path.replace(/.*\//, '') : "Empty selection"; }
            font.capitalization: Font.MixedCase
            font.pointSize: fontSizeInfo
            color:"white"
        }

        Slider {
            id:zoomId;
            scale: 0.8
            visible: imageView.visible
            //anchors.top: fileInfoId.bottom
            Layout.fillWidth: true
            Layout.margins: 2
            from: 0.1; to:1; value: pictureFrame.scale
        }

        Slider {
            id:sliderId;
            scale: 0.8
            visible: textViewId.text.length > 0 ? true : false
            // anchors.bottom: mRectangleId.bottom
            // width: parent.width
            padding: -5
            Layout.fillWidth: true
            Layout.margins: 2
            from: Qt.platform.os == "android" ? 10 : 7; to:15; value:11
        }
    }


    Rectangle {
        id:imageView
        visible: false
        Layout.alignment: Qt.AlignCenter
        Layout.fillWidth: true
        Layout.fillHeight: true
        color:"transparent"

        Flickable {
            id: flick_view
            anchors.fill: parent
            //anchors.top: controlPreviewId.bottom
            contentWidth: picture.width
            contentHeight: picture.height
            boundsBehavior: Flickable.StopAtBounds
            clip: false

            Rectangle {
                id: pictureFrame
                width: picture.width * (1 + 0.05 * picture.height / picture.width)
                height: picture.height * 1.05
                Behavior on scale { NumberAnimation { duration: 200 } }
                Behavior on x { NumberAnimation { duration: 200 } }
                Behavior on y { NumberAnimation { duration: 200 } }
                smooth: true
                antialiasing: true
                color:"transparent"

                Image {
                    id: picture
                    source : rootPreviewId.imageContent
                    anchors.centerIn: parent
                    anchors.alignWhenCentered: true
                    smooth: flick_view.moving
                    fillMode: Image.PreserveAspectFit
                    antialiasing: true
                }

                PinchArea {
                    anchors.fill: parent
                    pinch.target: pictureFrame
                    pinch.minimumRotation: -360
                    pinch.maximumRotation: 360
                    pinch.minimumScale: 0.1
                    pinch.maximumScale: 10
                    pinch.dragAxis: Pinch.XAndYAxis

                    onSmartZoom: {
                        if (pinch.scale > 0) {
                            pictureFrame.rotation = 0;
                            pictureFrame.scale = Math.min(rootPreviewId.width, rootPreviewId.height) / Math.max(picture.sourceSize.width, picture.sourceSize.height) * 0.85
                            pictureFrame.x = flick_view.contentX + (flick_view.width - pictureFrame.width) / 2
                            pictureFrame.y = flick_view.contentY + (flick_view.height - pictureFrame.height) / 2

                        } else {
                            pictureFrame.rotation = pinch.previousAngle
                            pictureFrame.scale = pinch.previousScale
                            pictureFrame.x = pinch.previousCenter.x - pictureFrame.width / 2
                            pictureFrame.y = pinch.previousCenter.y - pictureFrame.height / 2

                        }
                    }

                    MouseArea {
                        drag.target: pictureFrame
                        hoverEnabled: true
                        scrollGestureEnabled: false
                        anchors.fill: parent
                        onWheel: {
                            if (wheel.modifiers & Qt.ControlModifier) {
                                pictureFrame.rotation += wheel.angleDelta.y / 120 * 5;
                                if (Math.abs(pictureFrame.rotation) < 4)
                                    pictureFrame.rotation = 0;
                            } else {
                                pictureFrame.rotation += wheel.angleDelta.x / 120;
                                if (Math.abs(pictureFrame.rotation) < 0.6)
                                    pictureFrame.rotation = 0;
                                pictureFrame.scale += pictureFrame.scale * wheel.angleDelta.y / 120 / 10;
                            }
                        }
                    }
                }

                Connections {
                    target: zoomId
                    onValueChanged: pictureFrame.scale = zoomId.value
                }
            }

            states: State {
                name: "ShowBars"
                when: flick_view.movingVertically || flick_view.movingHorizontally
                PropertyChanges { target: verticalScrollBar; opacity: 1 }
                PropertyChanges { target: horizontalScrollBar; opacity: 1 }
            }

            transitions: Transition {
                NumberAnimation { properties: "opacity"; duration: 400 }
            }
        }

        // Attach scrollbars to the right and bottom edges of the view.
        MyScrollBar {
            id: verticalScrollBar
            width: 12; height: flick_view.height-12
            anchors.right: flick_view.right
            opacity: 0
            orientation: Qt.Vertical
            position: flick_view.visibleArea.yPosition
            pageSize: flick_view.visibleArea.heightRatio
        }

        MyScrollBar {
            id: horizontalScrollBar
            width: flick_view.width-12; height: 12
            anchors.bottom: flick_view.bottom
            opacity: 0
            orientation: Qt.Horizontal
            position: flick_view.visibleArea.xPosition
            pageSize: flick_view.visibleArea.widthRatio
        }


    }

    Rectangle {
        id : mRectangleId
        // width: 300
        height: Qt.platform.os == "android" ? 450 : 400
        color: "beige"; border.color: "yellowgreen"; border.width: 3; radius: 5
        visible: textViewId.text.length > 0 ? true : false
        Layout.fillWidth: true;
        Layout.fillHeight: true;
        Layout.margins: 2
        Flickable {
            id: flickable
            anchors.fill: parent
            contentWidth:  textViewId.width
            contentHeight: textViewId.height
            leftMargin: 5
            topMargin: 5
            clip: false

            Text {
                id : textViewId
                text: rootPreviewId.textContent
                width: parent.width
                font.pointSize: sliderId.value
                wrapMode: Text.WrapAnywhere
            }
        }

    }
}
