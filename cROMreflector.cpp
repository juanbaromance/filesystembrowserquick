
#include <QString>
#include <QDirIterator>
#include <QDebug>
#include <QStandardPaths>
#include "cROMreflectorImpl.h"
#include "cROMreflector.h"

#ifdef Q_OS_ANDROID
#include <QtAndroid>
#endif


cROMreflectorImpl::cROMreflectorImpl(){ reSample(); }

bool cROMreflectorImpl::reSample(){
    for( auto target : { QStandardPaths::writableLocation(QStandardPaths::AppDataLocation), externalStorage() } )
        if( target.length() )
            fileSystemLayoutReplicator( target );
    return true;
}

void cROMreflectorImpl::fileSystemLayoutReplicator(const QString &target)
{

    {
        QDirIterator it(":/rom/", QDirIterator::Subdirectories);
        while (it.hasNext()) {
            QFileInfo rdi(it.next());
            if( rdi.isDir() )
            {
                QFileInfo udi(target + rdi.filePath().split(":/rom").at(1) );
                if( udi.exists() )
                    continue;
                QDir ud(udi.filePath());
                if( ud.mkpath( udi.filePath()) )
                    qDebug() << ud.path() << " recreated";
            }
        }
    }

    {
        QDirIterator it(":/rom/", QDirIterator::Subdirectories);
        while (it.hasNext()) {
            QFileInfo rfi(it.next());
            if( rfi.isFile() )
            {
                QFileInfo ufi( target + rfi.filePath().split(":/rom").at(1) );
                if( ufi.exists() == false )
                {
                    QFile rf(rfi.filePath());
                    if( rf.copy( ufi.filePath()) )
                        qDebug() << ufi.filePath() << " recreated";
                }
            }
        }
    }
}

QString cROMreflectorImpl::externalStorage()
{
#ifdef Q_OS_ANDROID

    // auto backtrace = QAndroidJniObject::fromString(__PRETTY_FUNCTION__);
    // QAndroidJniObject::callStaticMethod<void>( package,"showDirs","(Ljava/lang/String;)V", backtrace.object<jstring>());

    auto androidContext = QtAndroid::androidContext();
    QAndroidJniObject dir = QAndroidJniObject::fromString(QString(""));
    QAndroidJniObject path = androidContext.callObjectMethod("getExternalFilesDir",
                                                             "(Ljava/lang/String;)Ljava/io/File;",
                                                             dir.object());
    return path.toString();
#endif
    return "";
}

#include <memory>
//std::unique_ptr<cROMreflectorImpl> impl;

#include <QQmlContext>
cROMreflector::cROMreflector(QQmlContext *context, QObject *parent) : QObject(parent)
{
    impl = std::make_unique<cROMreflectorImpl>();
    m_external_storage = impl->externalStorage();
    context->setContextProperty( "FileSystemWrapper", this );
}

bool cROMreflector::rebuild(){ return impl->reSample(); }

cROMreflector::~cROMreflector(){}

QString cROMreflector::external_storage() const { return m_external_storage; }
