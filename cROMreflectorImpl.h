#ifndef CROMREFLECTORIMPL_H
#define CROMREFLECTORIMPL_H

#include <QString>
class cROMreflectorImpl{
public:
    cROMreflectorImpl();
    bool reSample();
    virtual ~cROMreflectorImpl(){}
    QString externalStorage();

private:
    void  fileSystemLayoutReplicator(const QString & target);


};
#endif // CROMREFLECTORIMPL_H
