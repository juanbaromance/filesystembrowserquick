QT += quick
TARGET = FileSystemBrowser
CONFIG += c++17
CONFIG += qml_debug

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
	cROMreflector.cpp \
	cpp/fileIO.cpp \
	cpp/sharedApp.cpp \
        cpp/shareutils.cpp \
        main.cpp

android {

QT += androidextras
SOURCES += cpp/android/androidshareutils.cpp
HEADERS += cpp/android/androidshareutils.hpp
ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

}

RESOURCES += qml.qrc \
	qtResources.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
	android/AndroidManifest.xml \
	android/build.gradle \
	android/gradle/wrapper/gradle-wrapper.jar \
	android/gradle/wrapper/gradle-wrapper.properties \
	android/gradlew \
	android/gradlew.bat \
	android/res/values/libs.xml \
	android/src/org/ekkescorner/examples/sharex/QShareActivity.java \
	android/src/org/ekkescorner/utils/QSharePathResolver.java \
	android/src/org/ekkescorner/utils/QShareUtils.java \
	common/FloatingActionButton.qml \
	qtquickcontrols2.conf

HEADERS += \
	cROMreflector.h \
	cROMreflectorImpl.h \
	cpp/fileIO.h \
	cpp/iPlatform.h \
	cpp/sharedApp.hpp \
	cpp/shareutils.hpp
