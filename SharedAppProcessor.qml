import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import Qt.labs.folderlistmodel 2.12
import Qt.labs.platform 1.0
import QtQuick.Controls.Material 2.12
import FileSystemBrowser 1.0

Item {
    id: sharedAppProcessor
    property int viewRequest: 1

    Popup {
        id: popup
        closePolicy: Popup.CloseOnPressOutside | Popup.CloseOnEscape
        x: 16
        y: 100
        implicitHeight: columnId.implicitHeight*1.2
        implicitWidth: appWindow.width * .9

        property alias labelText: popupLabel.text
        Column {
            id:columnId
            anchors.right: parent.right;anchors.left: parent.left
            spacing: 20
            Label {
                id: popupLabel
                topPadding: 8;leftPadding: 8;rightPadding: 8
                width: parent.width
                text: ""
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }
            Button {
                id: okButton
                text: "OK"
                onClicked: popup.close()
            }
        }
    }

    Connections {
        target: shareUtils
        onShareFinished: onShareFinished(requestCode)
    }

    function onShareFinished( requestCode ) {
        // popup.labelText = "HelperApp done<br>" + shareUtils.auditory();
        // popup.open()
        requestCanceledOrViewDoneOrSendDone( requestCode )
    }

    function requestCanceledOrViewDoneOrSendDone(requestCode) { myApp.recycleSharedFolder() }

    Connections {
        target: shareUtils
        onShareNoAppAvailable: onShareNoAppAvailable(requestCode)
    }

    function onShareNoAppAvailable(requestCode) {
        console.log ("HelperApp not available: "+ requestCode )
        const services = new Map();
        services.set( SharedAppEnums.ViewId, "View")
        services.set( SharedAppEnums.SendId, "Send")
        services.set( SharedAppEnums.EditId, "Edit")
        var service = services.get(requestCode);
        service = ( service === undefined ) ? "unknown service" : service;

        popup.labelText = mtime + ": HelperApp not available for <b>" + service + "</b> service<br><br>" + "<small>" + shareUtils.auditory() + "</small>";
        popup.open()
        requestCanceledOrViewDoneOrSendDone(requestCode)

//        preViewId.visible = true
//        swipeView.setCurrentIndex(1);
        return;
    }

    Connections {
        target: shareUtils
        onShareError: onShareError(message, requestCode )
    }
    function onShareError(message, requestCode )
    {
        console.log ("share error("+ requestCode + ")" + message)
        if( requestCode === SharedAppEnums.ViewId ) {
            popup.labelText = "(View File) " + message + shareUtils.auditory();
            popup.open()
            requestCanceledOrViewDoneOrSendDone(requestCode)
            return
        }

        popup.labelText = "<b>" + mtime + "</b>: " + message
        popup.open()
        return;
    }
}
