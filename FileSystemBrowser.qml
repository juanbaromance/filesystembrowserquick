import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import Qt.labs.folderlistmodel 2.12
import Qt.labs.platform 1.0
import "common"

ListView {
    
    id: listViewId
    anchors.fill: parent
    anchors.margins: 5
    orientation: ListView.Vertical
    signal acceptedSelection( var fileName )
    property alias filters: folderModelId.nameFilters
    property alias folderSel: folderModelId.folder
    property string modePreview;

    function extension(filename) {  return filename.slice((Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1) }
    spacing: 5
    Layout.margins: 5
    focus: true
    ScrollIndicator.vertical: ScrollIndicator { }
    headerPositioning: ListView.OverlayHeader
    model: folderModelId

    header :
        ColumnLayout {

        Layout.fillWidth: true
        width: parent.width

        RowLayout{

            //            Layout.fillWidth: true
            width: parent.width

            Button {
                visible: Qt.platform.os == "android"
                onClicked:
                {
                    console.log( "External Storage: " + FileSystemWrapper.external_storage )
                    listViewId.folderSel = "file://"  + FileSystemWrapper.external_storage
                }
                icon.source : "qrc:/Images/simcard.png";
            }

            Button {
                onClicked: listViewId.folderSel = StandardPaths.writableLocation(StandardPaths.AppDataLocation)
                icon.source : "qrc:/Images/home.png"
            }

            Button {
                onClicked:listViewId.folderSel = "qrc:/rom"
                icon.source : "qrc:/Images/rom.png"
            }

            Button {
                readonly property string usability: "<b>Reconstructs</b> user File systems with <b>ROM</b> contents"
                property var feedback: usability
                icon.source : "qrc:/Images/rebuild.png"
                onClicked: {
                    feedback += "<br>" + mtime + " : Layout <b>" + ( FileSystemWrapper.rebuild() ? "successfully" : "failure on" ) + "</b> rebuilding"
                    feedback = ( feedback.length > 512 ) ? usability : feedback
                }
                hoverEnabled: true
                ToolTip.delay: 1000
                ToolTip.timeout: 5000
                ToolTip.visible: down|hovered
                ToolTip.text: feedback
                // Lets explain this runtime error: ToolTip.font.pointSize: 10
            }

            Switch {
                visible: Qt.platform.os == "android" ? true : true
                text:"Internal"
                onPositionChanged: listViewId.modePreview = text = visualPosition === 0 ? "Internal" :"External"
            }
        }


        TextField {
            id: whereIamId
            width: parent.width
            Layout.fillWidth: true
            placeholderText: "Host folder"
            selectByMouse: true
            text: listViewId.folderSel
            font.pointSize: Qt.platform.os == "android" ? 12 : 10
            onAccepted: listViewId.folderSel = text
        }

    }
    
    FolderListModel {
        id: folderModelId
        showDirs: true
        showDirsFirst: true
       // showDotAndDotDot: true
        nameFilters: ["*"]
        rootFolder:StandardPaths.writableLocation(StandardPaths.AppDataLocation)
    }
    
    delegate : SwipeDelegate {
        id : folderDelegateId
        width: parent.width
        property var suffix : String(fileSuffix).toLowerCase().split('.').pop();
        property var fileLabelId: filenameId

        Image {
            id: iconId
            opacity: 1 - folderDelegateId.swipe.position
            source: "qrc:/Images/FileBrowser/" + resolveIcon() + ".png";

            function resolveIcon()
            {
                if ( fileIsDir )
                    return "folder-documents";
                const mimes = new Map();
                mimes.set( "xml", "text-xml"               )
                mimes.set( "png" , "image-x-png"            )
                mimes.set( "jpg" , "image-x-png"            )
                mimes.set( "jpeg", "image-x-png"            )
                mimes.set( "pdf", "application-x-pdf"      )
                mimes.set( "sql", "application-sql"        )
                mimes.set( "bin", "application-x-firmware" )
                mimes.set( "sqlite", "application-sql")
                mimes.set( "ini", "text-ini")
                mimes.set( "zip", "application-zip")
                mimes.set( "md", "text-markdown")
                mimes.set( "so", "application-octet-stream")
                mimes.set( "qml", "text-x-qml")
                mimes.set( "tgz", "mime-x-tar")
                mimes.set( "jar", "application-x-java-archive")

                var extension = mimes.get(listViewId.extension(filenameId.text));
                if( extension === undefined )
                    return "text-x-generic";
                return extension
            }

            height: 48; width: 48
            // scale: 0.8

        }

        Text {
            id : filenameId
            width : parent.width
            height: parent.height
            anchors.left: iconId.right
            text: fileName
            color:"white"
            font.pointSize: Qt.platform.os == "android" ? 14 : 11
            font.bold: [ "xml","ini","txt" ].find(item => item === folderDelegateId.suffix ) ? true :false
            verticalAlignment: Text.AlignVCenter
        }

        onClicked:
        {
            console.log(mtime + ": Clicked")
            if ( fileIsDir )
            {
                folderModelId.folder = fileURL
                console.log("back points to: ",fileURL);
            }
            else
            {
                elapsedTimer.reset();
                acceptedSelection( fileURL );

                console.log("URL()" + fileURL );
                console.log("path()" + filePath )
                console.log("filename()" + fileName )

                // if( ["png","jpg","txt","ini","log","xml"].find( ext => ext === suffix )  )

            }
        }

        swipe.left: Rectangle {
            Image {
                opacity: swipe.position
                source : "Images/removeFile.png"
                height: 48; width: 48
                // scale: 0.8
            }
            width: parent.width
            height: parent.height
            radius: 5
            color: SwipeDelegate.pressed ? "transparent" : "SteelBlue"
            MouseArea {
                anchors.fill: parent
                onClicked:
                {
                    swipe.close()
                    filenameId.text = fileName;
                    undoTimer.stop();
                    elapsedTimer.reset();
                }
            }

// JB#05122020
// This signal doesn't work properly on mobile devices, the above MouseArea is the portable way to handle
// clicks on the swipe
//            SwipeDelegate.onClicked:
//            {
//                console.log(mtime + ": Delete cancelled")
//                swipe.close()
//                filenameId.text = fileName;
//                undoTimer.stop();
//                elapsedTimer.reset();
//            }
        }


        swipe.onCompleted: {
            if ( fileIsDir )
            {
                swipe.close()
                return;
            }
            filenameId.text = fileName + " <i>will be removed on 4000 sec</i><br><b>Click here</b> to <b>cancel</b> deletion"
            undoTimer.start()
            elapsedTimer.init()
        }

        ProgressBar
        {
            id:pb
            from: 0; to: 100;
            Layout.fillWidth: true
            anchors.left: iconId.right
            visible: false
            anchors.margins: 2
        }

        // @disable-check M300
        Timer {
            id: elapsedTimer

            interval:100
            repeat:true
            property int elapsed: 0
            property int deadline: 3600
            function reset()
            {
                elapsed = 0;
                pb.visible = false;
                stop();
            }

            function init( value = 0)
            {
                reset();
                pb.visible = true;
                start();
            }

            onTriggered: {
                elapsed += interval
                pb.value = 100 * ( elapsed/ deadline );
                if( elapsed > deadline )
                {
                    stop()
                    elapsed = 0;
                    pb.visible = false;
                }
            }
        }

        // @disable-check M300
        Timer {
            id: undoTimer
            interval: 3600
            onTriggered: {
                elapsedTimer.reset();
                filenameId.text = fileName + " <b>Deleted<b> on " + mtime;
                iconId.opacity = 0
                folderDelegateId.swipe.close();
            }
        }

    }


}
